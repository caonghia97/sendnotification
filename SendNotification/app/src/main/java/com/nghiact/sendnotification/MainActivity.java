package com.nghiact.sendnotification;


import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = MainActivity.class.getName();
    private EditText editTitle;
    private EditText editMessage;
    private EditText editToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
    }

    private void initView() {
        FirebaseMessaging.getInstance().subscribeToTopic("all");
        editTitle = findViewById(R.id.edit_title);
        editMessage = findViewById(R.id.edit_message);
        editToken = findViewById(R.id.edit_token);
        findViewById(R.id.bt_push_notification).setOnClickListener(this);
        findViewById(R.id.bt_get_token).setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.bt_push_notification) {
            pushNotifiCation();
        } else if (v.getId() == R.id.bt_get_token) {
            getToken();
        }
    }

    private void pushNotifiCation() {
        String title = editTitle.getText().toString();
        String message = editMessage.getText().toString();
        String token = editToken.getText().toString();

        FcmNotificationsSender notificationsSender = new FcmNotificationsSender(token,
                title, message, getApplicationContext(), MainActivity.this);
        notificationsSender.SendNotifications();
    }

    private void getToken() {
        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(new OnCompleteListener<String>() {
                    @Override
                    public void onComplete(@NonNull Task<String> task) {
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "Fetching FCM registration token failed", task.getException());
                            return;
                        }

                        // Get new FCM registration token
                        String token = task.getResult();
                        // Log and toast
                        Log.d(TAG, token);
                        editToken.setText(token);

                    }
                });
    }
}